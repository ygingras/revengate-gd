Godot RPG Proof of Concept
==========================
This is a proof of concept for a turn-based dungeon crawler. The goal is to test Godot as a possible platform to re-implement Revengate.

More about Revengate:
* [Website](http://revengate.org)
* [Git repo](https://gitlab.com/ygingras/revengate/)
* Google Play ([here](https://play.google.com/store/apps/details?id=org.revengate.revengate) or [here](https://play.google.com/apps/testing/org.revengate.revengate))
